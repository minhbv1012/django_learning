from django.db import models


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    data = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='media/')
