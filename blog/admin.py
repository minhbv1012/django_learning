from django.contrib import admin
from .models import Post
# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display=['title','data']
    list_filter = ['data']
    search_fields = ['title']
    show_full_result_count = 20
admin.site.register(Post,PostAdmin)
